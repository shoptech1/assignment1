const Order = require("./assignment1Order");

const OrderState = Object.freeze({
    WELCOMING:   Symbol("welcoming"),
    CHOICE: Symbol("choice"),
    SIZE:       Symbol("size"),
    ADDONS:   Symbol("addOns"),
    DESSERTS:  Symbol("desserts")
});

let price;
let deserts;
let myArray;
let smallPrice = 6;
let mediumPrice = 10;
let largePrice = 15;
let eachAddOn = 1.5;
let eachDessert = 4;

module.exports = class PastaOrder extends Order{
    constructor(){
        super();
        this.stateCur = OrderState.WELCOMING;
        this.sChoice = "";
        this.sSize = "";
        this.sAddOns = "";
        this.sDesserts = "";
        // 2 variables with same name is written to avoid error occured due to case sensitivity
        this.sItem1 = "Pasta";
        this.sItem1 = "pasta";
        this.sItem2 = "Poutine";
        this.sItem2 = "poutine";
        this.sItem3 = "Sandwich";
        this.sItem3 = "sandwich";

    }
    handleInput(sInput){
        let aReturn = [];
        switch(this.stateCur){
            case OrderState.WELCOMING:
                this.stateCur = OrderState.CHOICE;
                aReturn.push("Welcome to Aanal's Pasta House.");
                aReturn.push("What would you like to have - Pasta, Poutine or Sandwich?");
                break;
            case OrderState.CHOICE:
                this.stateCur = OrderState.SIZE
                this.sChoice = sInput;
                aReturn.push("What size would you like?");
                break;
            case OrderState.SIZE:
                this.stateCur = OrderState.ADDONS
                this.sSize = sInput;
                aReturn.push("What add Ons would you like?");
                break;
            case OrderState.ADDONS:
                this.stateCur = OrderState.DESSERTS
                this.sAddOns = sInput;
                aReturn.push("Which desserts would you like?");
                break;
            case OrderState.DESSERTS:
                this.isDone(true);
                if(sInput.toLowerCase() != "no"){
                    this.sDesserts = sInput;
                }
                aReturn.push("Thank-you for your order of");
                if(this.sChoice==this.sItem1){
                    aReturn.push(`${this.sSize} ${this.sItem1} with ${this.sAddOns}`);
                }
                else if(this.sChoice==this.sItem2){
                    aReturn.push(`${this.sSize} ${this.sItem2} with ${this.sAddOns}`); 
                } 
                else{
                    aReturn.push(`${this.sSize} ${this.sItem3} with ${this.sAddOns}`); 
                }  

                if(this.sDesserts){
                    aReturn.push(this.sDesserts);
                }
                let d = new Date(); 
                d.setMinutes(d.getMinutes() + 20);
                aReturn.push(`Please pick it up at ${d.toTimeString()}`);

                let size = this.sSize;
                if(size=="small" || size=="Small")
                {
                    price = smallPrice;
                }
                else if(size=="medium" || size=="Medium")
                {
                    price = mediumPrice;
                }
                else{
                    price = largePrice;
                }

                myArray = this.sAddOns.split(",");
                var count = 0;
                for(var i = 0; i < myArray.length; ++i){
                    count++;
                }
                price += count*eachAddOn;

                if(this.sDesserts!=""){
                    deserts = this.sDesserts.split(",");
                }
                var desertCount = 0;
                for(var i = 0; i < deserts.length; ++i){
                    desertCount++;
                }
                price += desertCount*eachDessert;

                aReturn.push(`Your total price is $ ${price}`)
                break;
                
        }
        return aReturn;
    }
}