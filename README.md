# README #

 How to build install and use my project:
 
 Steps:
 
 1. Firstly, you must have node.js installed in your system.
 2. Clone this repository in Visual Studio Code.
 3. Open terminal, write npm install.
 4. Then, node index.js.
 5. Open browser and write localhost:3002.
 6. You can see the project running.
 
 About LICENSE:
 
 I have added Apache License 2.0 because it is an OSI-approved license and it is popular, widely used, and have strong communities.